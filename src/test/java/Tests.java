import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class Tests {

    ChromeDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver,10,500);

    @Before
    public void beforeTests(){
        System.setProperty("webdriver.chrome.driver", "C:/Users/User/Documents/Java projects/YandexMarketAutoTestForAlpha/chromedriver.exe");
        driver.manage().window().maximize();
        driver.get("http://yandex.ru");
        driver.findElement(By.cssSelector("[data-id='market']")).click();
        driver.findElement(By.cssSelector("[href='/catalog/54440']")).click();
    }

    //общий метод для первых двух тестов, т.к. они похожи
    public void mainTest(String typeOfDevices, String creator1, String creator2, String wasteWord, String price, boolean checkTheNumberOfItems) throws InterruptedException {
        driver.findElement(By.cssSelector(typeOfDevices)).click();
        driver.findElement(By.id("glpricefrom")).sendKeys(price);
        driver.findElement(By.cssSelector(creator1)).click();
        if (!creator2.equals("-")) {
            driver.findElement(By.cssSelector(creator2)).click();
        }

        //понимаю, что использовать задержку не хорошо, но в данном контексте, я не нашел подходящего элемента для привязки к ожиданию
        Thread.sleep(5000);

        if (checkTheNumberOfItems==true){
            Assert.assertTrue(driver.findElements(By.className("n-snippet-cell2__body")).size()==48);
        }

        WebElement element = driver.findElement(By.className("n-snippet-cell2__title")).findElement(By.tagName("a"));
        String titleFirstElement = element.getAttribute("title");
        String nameFirstElement;
        nameFirstElement = titleFirstElement.replace(wasteWord, "");

        driver.findElement(By.id("header-search")).sendKeys(nameFirstElement);
        driver.findElement(By.className("search2__button")).click();

        element = driver.findElement(By.tagName("h1"));
        String titleMainElement = element.getText();

        Assert.assertTrue(titleMainElement.equals(titleFirstElement));
    }


    @Test
    public void firstTest() throws InterruptedException {
        mainTest("[href='/catalog--mobilnye-telefony/54726/list?catId=91491&hid=91491']",
                "[for='7893318_153043']",
                "[for='7893318_153061']",
                "Смартфон",
                "20000",
                true);
    }

    @Test
    public void secondTest() throws InterruptedException {
        mainTest("[href='/catalog--naushniki-i-bluetooth-garnitury/56179/list?catId=90555&hid=90555']",
                "[for='7893318_8455647']",
                "-",
                "Наушники",
                "500",
                false);
    }

    @Test
    public void thirdTest() throws InterruptedException {
        driver.findElement(By.cssSelector("[href='/catalog--mobilnye-telefony/54726/list?catId=91491&hid=91491']")).click();
        driver.findElement(By.tagName("a")).findElement(By.xpath("//*[.='по цене']")).click();
        Thread.sleep(5000);
        //собираем вместе все нужные нам цены
        List<WebElement> myElements = driver.findElements(By.cssSelector("div[class='price']"));
        int amountOfPrices = myElements.size();

        String[] prices = new String[amountOfPrices];
        int[] intPrices = new int[amountOfPrices];
        int i = 0;

        //избавляемся от "рублей" и переводим массив в числовой
        for(WebElement e : myElements) {
            prices[i] = e.getText();
            prices[i] = prices[i].replace(" \u20BD", "");
            intPrices[i] = Integer.parseInt(prices[i]);
            //System.out.println(intPrices[i]);
            i=i+1;
        }
        //проверяем соответствие сортировки
        for(i=1;i<amountOfPrices;i++) {
            Assert.assertTrue(intPrices[i-1]<=intPrices[i]);
        }
    }

    @After
    public void afterTests(){
        driver.quit();
    }
}
